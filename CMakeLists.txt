cmake_minimum_required (VERSION 2.8)
project (libg)
set(CMAKE_CXX_FLAGS " -Wall -Werror -std=c++11 ${CMAKE_CXX_FLAGS}")

add_subdirectory (lib)
add_subdirectory (examples)


