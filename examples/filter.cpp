#include <iostream>
#include "Rect.hpp"
#include "Line.hpp"

#include "Svg.hpp"

using namespace libg;

int main (int argc, char *argv[]){
  
  Svg svg(Width(640), Height (480));
  
  Style style;
  style.dropShadow();
  style.setFill (Color (Color(R(128), G(128), B(255))));
  style.setStroke (Color (R(75), G(75), B(200)), Width(2));

  Rect rect (X(10), Y(10), Width(100), Height(60));
  Line line ({{X(0),Y(0)},{X(10),Y(10)},{X(20),Y(40)},{X(30),Y(90)}});

  line.setStyle(style);
  rect.setStyle(style);

  rect.setRx(5);

  rect.setFilter("dropShadow");

  svg << rect << line;

  svg.print();

  
  return 0;
}
