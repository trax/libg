#define CLASS_PATH "lib/Rect.cpp"
#include <iostream>
#include <sstream>
#include "Rect.hpp"
#include "Svg.hpp"
#include "Style.hpp"
#include "Text.hpp"

using namespace libg;

void stream(){
  
  Svg svg(Width(640), Height (480));
  Color lightBlue (R(128), G(128), B(255));
  Color darkBlue  (R(32), G(32), B(200));


  Style style;
  style.setFill (lightBlue);
  style.setStroke (darkBlue, Width(2));

  Rect rect0 (X(10), Y(10), Width(100), Height(60));
  Rect rect1 (rect0);

  rect0.setStyle(style);
  rect1.setStyle(style);

  rect1.setRx(5);
  rect1.setY(Y(80));

  Text text (Point(X(200),Y(200)), "Hello world!");

  svg << rect0 << rect1 << text;

  svg.print();

}


int main (int argc, char *argv[]){

  //append();
  stream();
  
  return 0;
}
  
