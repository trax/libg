#include "Line.hpp"
#include "Svg.hpp"
#include "Style.hpp"



using namespace libg;

void segment(Svg &svg){
  Style style;
  style.setStroke(Color (R(255), G(128), B(128)), Width(1));

  Line line (X(0),Y(10),
             X(100),Y(10));
  line.setStyle(style);
  
  svg << line;
}


void polyline(Svg &svg){
  Style style;
  style.setStroke(Color (R(128), G(128), B(255)), Width(1));

  Line line ({{X(0),Y(0)},{X(10),Y(10)},{X(20),Y(40)},{X(30),Y(90)}});
  line.setStyle (style);
  

  svg << line;
}


int main (int argc, char *argv[]){
  Svg svg(Width(100), Height (100));

  segment(svg);
  polyline(svg);
  svg.print();
  
  return 0;
}
