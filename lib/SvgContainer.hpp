#ifndef LIBG_SVGCONTAIER_H_
#define LIBG_SVGCONTAIER_H_

#include <sstream>
#include "CommonTypes.hpp"

namespace libg {
class SvgElement;


class SvgContainer {
  private:
  std::stringstream container;
  
  public:
  explicit SvgContainer();

  SvgContainer& operator<< (const SvgElement &next);
  SvgContainer& operator<< (const std::string &next);
  SvgContainer& operator<< (const SvgContainer &next);
  
  template <typename T> 
  SvgContainer & operator<< (const Attribute<T> &attribute){
    if(attribute){

      if (attribute.type == Attribute<T>::Type::STYLE){
        container << attribute.prefix << ":"
                  << static_cast<std::string>(attribute.data) 
                  << attribute.suffix << ";"; 
      } else {
        container << attribute.prefix << "=\""
                  << static_cast<std::string>(attribute.data) 
                  << attribute.suffix << "\" "; 
      }
    }

    return *this;
  }

  const std::string str() const;

  void clear();
};


} // libg

#endif /* LIBG */
