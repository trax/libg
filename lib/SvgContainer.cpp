#include <sstream>
#include <iostream>
#include "SvgContainer.hpp"
#include "SvgElement.hpp"

namespace libg {

SvgContainer::SvgContainer(){}

SvgContainer& SvgContainer::operator<< (const SvgElement &next) {
  next.compute (this);
  return *this;
}

SvgContainer&  SvgContainer::operator<< (const std::string &next){
  container << next;
  return *this;
}

SvgContainer& SvgContainer::operator<< (const SvgContainer &next) {
  container << next.str();
  return *this;
}

const std::string SvgContainer::str() const{
  return container.str();
}

void SvgContainer::clear(){
  container.str("");
}



} // libg
