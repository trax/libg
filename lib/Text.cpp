#include "Text.hpp"


namespace libg {

Text::Text(Point position, const std::string &text, TextAnchor _anchor):
  text(text),
  position(position) {
  setAnchor(_anchor);
}


void Text::setAnchor (TextAnchor _anchor) {
  std::string anchorType;
  switch (_anchor){
  case TextAnchor::START:
    anchorType = "start";
    break;
  case TextAnchor::MIDDLE: 
    anchorType = "middle";
    break;
  case TextAnchor::END: 
    anchorType = "end";
    break;
  case TextAnchor::INHERIT: 
    anchorType = "inherit";
    break;
  }

  anchor.set(anchorType, "text-anchor");
}



bool Text::compute(SvgContainer *container) const {
  std::string anchorType; 

  
  (*container) << "<text "
               << position
               << style  
               << anchor 
               << ">"
               << text << "</text>";

  return true;
}


} // libg

