#include <sstream>
#include "Rect.hpp"



namespace libg {
Rect::Rect(const Rect &rect) :
  x(rect.x),
  y(rect.y),
  w(rect.w),
  h(rect.h),
  rx(rect.rx){
}


Rect::Rect(X x, Y y, Width w, Height h, RX rx) :
  x(x, "x"), y(y, "y"), 
  w(w, "width"), h(h, "height"), 
  rx(rx, "rx"){}


void Rect::setRx(uint16_t _rx){rx.data = _rx;}
void Rect::setWidth(Width _w){w.data = _w;}
void Rect::setHeiht(Height _h){h.data = _h;}
void Rect::setX(X _x){x.data = _x;}
void Rect::setY(Y _y){y.data = _y;}


bool Rect::compute (SvgContainer *container) const{
  
  (*container) << "<rect "
               << x << y
               << w << h
               << rx
               << style
               << "/> ";

  return true;
}
  

} // libg


