#include <sstream>
#include "Color.hpp"

namespace libg {


Color::Color():
  r(0), g(128), b(255){}

Color::Color(R r, G g, B b) :
  r(r), g(g), b(b){}


void Color::operator= (const Color &color) {
  r = color.r;
  g = color.g;
  b = color.b;
}

Color::operator std::string () const {
  std::string s("rgb(");
  s.append(r);
  s.append(",");
  s.append(g);
  s.append(",");
  s.append(b);
  s.append(")");

  return s;
}

} // libg
