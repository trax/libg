#include <sstream>
#include <iostream>
#include <memory>
#include "SvgContainer.hpp"
#include "Svg.hpp"

namespace libg {


Svg::Svg(Width w, Height h):w(w), h(h){

  layers.push_back(std::unique_ptr<SvgContainer>(new SvgContainer()));
}


SvgContainer& Svg::getNextLayer() {
  layers.push_back(std::unique_ptr<SvgContainer>(new SvgContainer()));
  return *layers.back();
}


SvgContainer& Svg::append(const libg::SvgElement &element) {
  (*layers[0].get()) << element;
  return *(layers[0].get());
}


void Svg::compute(SvgContainer *s){
  (*s) << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" "
       << "width=\"" << w << "\" height=\"" << h << "\" >";
  
  Style::setDefs(s);

  (*s) << "<g>";
  for (auto it = layers.begin() , end = layers.end() ; it != end ; ++it){
    (*s) << it->get()->str();
  }
  (*s) << "</g>";


  (*s) << "</svg>" ;

}

void Svg::print() {
  SvgContainer s;
  compute (&s);
  std::cout << s.str() << std::endl;
}


SvgContainer& Svg::operator<< (SvgElement &next){
  return append (next);
}



} // libg
