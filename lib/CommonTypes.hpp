#ifndef LIBG_COMMONTYPES_H_
#define LIBG_COMMONTYPES_H_

#include <cstdint>
#include <string>

#include "CommonTypes.hpp"


namespace libg {


using std::uint32_t;
using std::uint8_t;


struct Stringable{
  virtual operator std::string() const = 0;
};

// extracted from BOOST
#define STRONG_TYPEDEF(T, D)                                  \
  struct D : public Stringable {                              \
  T t;                                                        \
  explicit D(const T t_) : t(t_) {};                          \
  D(){};                                                      \
  D(const D & t_) : t(t_.t){}                                 \
  D & operator=(const D & rhs) { t = rhs.t; return *this;}    \
  D & operator=(const T & rhs) { t = rhs; return *this;}      \
  /*operator const T & () const {return t; }*/                \
  operator T & () { return t; }                               \
  bool operator==(const D & rhs) const { return t == rhs.t; } \
  bool operator<(const D & rhs) const { return t < rhs.t; }   \
  operator std::string() const;                               \
  };

STRONG_TYPEDEF(uint32_t, Width );
STRONG_TYPEDEF(uint32_t, Height);
STRONG_TYPEDEF(uint32_t, X     );
STRONG_TYPEDEF(uint32_t, Y     );
STRONG_TYPEDEF(uint8_t, R);
STRONG_TYPEDEF(uint8_t, G);
STRONG_TYPEDEF(uint8_t, B);
STRONG_TYPEDEF(float,  RX);


template <typename T>
class Attribute{
private: 
  bool isSet;
    
public:
  enum class Type {
    ATTRIBUTE,STYLE
  };

  T data;
  std::string prefix;
  std::string suffix;
  Type type;

  Attribute(Type type = Type::STYLE):isSet(false){}
    
  Attribute (const T &_data,
             const std::string _prefix,
             const std::string _suffix = "",
             Type type = Type::ATTRIBUTE):
    isSet(true),
    data(_data),
    prefix(_prefix),
    suffix(_suffix),
    type(type)
  {}


  void set (const T &_data,
            const std::string _prefix,
            const std::string _suffix = "",
            Type _type = Type::ATTRIBUTE){
    data   = _data;
    prefix = _prefix;
    suffix = _suffix;
    type   = _type;
    isSet  = true;
  }
  
  operator bool() const {
    return isSet;
  }
};


} // libg


#endif /* LIBG */
