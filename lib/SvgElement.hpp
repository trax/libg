#ifndef LIBG_SVGELEMENT_H_
#define LIBG_SVGELEMENT_H_

#include "CommonTypes.hpp"
#include "Style.hpp"

namespace libg {


class SvgElement {

protected:
  Style style;

public:
  typedef std::string Element;
  virtual void setStyle (const Style &style);
  virtual Style &getStyle();
  virtual bool compute(SvgContainer *) const = 0; 
  virtual void setFilter(const std::string &filterName);
  
};


} // libg

#endif /* LIBG */
