#ifndef LIBG_STYLE_H_
#define LIBG_STYLE_H_
#include <vector>
#include <string>
#include <iostream>
#include <list>

#include "Color.hpp"
#include "CommonTypes.hpp"

namespace libg {


class Style {
private:
  static std::vector<std::string> defs;



  Attribute<Color>       fill;
  Attribute<Color>       strokeColor;
  Attribute<Width>       strokeWidth;
  Attribute<std::string> filterName;
  Attribute<float>       opacity;

  template <typename T>
  void addAttribute(SvgContainer *_container,
                    const Attribute<T> &attribute) const{
    if (attribute) {
      (*_container) << attribute.prefix 
                    << attribute.data
                    << attribute.suffix
                    << ";";
    }
  }



public:
  

  explicit Style (bool setDefault =  false);

  void setFill(const Color &color);
  void setStroke(const Color &color, Width w);
  void setFilter(const std::string &filterName);
  void setOpacity(float f);

  static void addDefs(const std::string &filter);
  static void setDefs(SvgContainer *container);
  
  void compute (SvgContainer *_container) const;  
  
  void dropShadow();
  
};

SvgContainer& operator<< (SvgContainer& container, const Style &style);



} // libg

#endif /* LIBG */
