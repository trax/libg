#ifndef LIBG_RECT_H_
#define LIBG_RECT_H_



#include "CommonTypes.hpp"
#include "SvgElement.hpp" 
#include "Color.hpp"

namespace libg {

class Rect :public SvgElement {
private:
  Attribute<X>      x;
  Attribute<Y>      y;
  Attribute<Width>  w; 
  Attribute<Height> h;
  Attribute<RX>    rx;

public:
  Rect(const Rect &);
  Rect(X x, Y y, Width w, Height h, RX rx = {});
  void setRx(uint16_t _rx);
  void setWidth(Width _w);
  void setHeiht(Height _h);
  void setX(X _x);
  void setY(Y _y);
  bool compute(SvgContainer *) const ;
};

} // libg

#endif /* LIBG */
