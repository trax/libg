#include "Point.hpp"

namespace libg {

SvgContainer& operator<< (SvgContainer &container, Point point) {
  container << "x=\"" << point.x 
            << "\" y=\"" << point.y 
            << "\" ";

  return container;
} 


} // libg
