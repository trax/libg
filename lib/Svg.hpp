#include <vector>
#include <memory>
#include "CommonTypes.hpp"
#include "SvgElement.hpp"

namespace libg {


class Svg {
private:
  Width w;
  Height h;
  std::vector <std::unique_ptr<SvgContainer> > layers;
  SvgContainer ss;

public:
  
  Svg(Width w, Height h);

  SvgContainer& getNextLayer();
  SvgContainer& append(const SvgElement &element);

  void compute(SvgContainer *s);

  SvgContainer& operator<< (SvgElement &next);
  void print();


};



} // libg
