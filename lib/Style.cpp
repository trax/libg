#include <sstream>
#include <string>
#include "Style.hpp"
#include "Color.hpp"
#include <iostream>
namespace libg {

Style::Style (bool setDefault){
  if (!setDefault){
    return;
  }

  setFill(Color (R(200), G(200), B(255)));
  setStroke(Color (R(100), G(100), B(255)), Width(3));
  
}



void Style::setFill(const Color &color) {
  fill.set(color, "fill","",fill.Type::STYLE);
}

void Style::setStroke(const Color &color, 
                   Width w){
  strokeColor.set(color, "stroke","",strokeColor.Type::STYLE);
  strokeWidth.set(w, "stroke-width","",strokeWidth.Type::STYLE); 
}

void Style::setOpacity(float f){
  opacity.set(f, "opacity","",opacity.Type::STYLE);
}

void Style::compute (SvgContainer *_container) const{
  (*_container) << "style=\"";


  (*_container) << filterName  
                << fill 
                << strokeColor
                << strokeWidth 
                << "\" ";
}


void Style::addDefs(const std::string &def) {
  defs.push_back(def);
}

void Style::setDefs(SvgContainer *container){
  (*container) << "<defs>";
  for (const auto def : defs) {
    (*container) << def << " ";
  }
  (*container) << "</defs>";
}

void Style::setFilter(const std::string &_filterName) {
  filterName.set(_filterName, "filter:url(#",")", filterName.Type::STYLE);
}


void Style::dropShadow(){
  addDefs("<filter id=\"dropShadow\" width=\"200%\" height=\"200%\">"
          "<feGaussianBlur in=\"SourceAlpha\"  stdDeviation=\"2\" result=\"blur\"/>"
          "<feOffset  in=\"blur\" dx=\"3\" dy=\"3\" result=\"offset\" />"
          "<feBlend in=\"SourceGraphic\" in2=\"offset\" mode=\"normal\" />"
          "</filter>");
}


SvgContainer& operator<< (SvgContainer& container, const Style &style) {
  style.compute (&container);
  return container;
}


std::vector<std::string> Style::defs = {};


} // libg
