#ifndef LIBG_COLOR_H_
#define LIBG_COLOR_H_

#include "CommonTypes.hpp"
#include "SvgContainer.hpp"


namespace libg {


class Color {
public:

private:
  R r;
  G g;
  B b;

public:

  Color();
  Color(R r, G g, B b);
  void operator= (const Color &color);
  void compute (SvgContainer *container) const;
  operator std::string() const;
};


} // libg

#endif /* LIBG */
