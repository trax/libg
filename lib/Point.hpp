#ifndef LIBG_POINT_H_
#define LIBG_POINT_H_

#include "CommonTypes.hpp"
#include "SvgContainer.hpp"

namespace libg {


struct Point {
  X x;
  Y y;
  Point(X x, Y y) :x(x), y(y){}
  
};


SvgContainer & operator<< (SvgContainer &container, Point point); 

} // libg

#endif /* LIBG */
