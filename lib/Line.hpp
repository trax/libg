#ifndef LIBG_LINE_H_
#define LIBG_LINE_H_

#include <vector>
#include "CommonTypes.hpp"
#include "SvgElement.hpp" 
#include "Point.hpp"

namespace libg {


class Line : public SvgElement {
public:
    typedef std::vector <Point> Points;

private:
  Points points;

public:
  typedef  std::iterator<std::input_iterator_tag, 
                         Point> iterator;


  Line(Point p0, Point p1);
  Line(X x0, Y y0, X x1, Y y1);

  Line(Points points);

  template <typename IT>
  Line(IT begin, IT end) {
    while (begin != end) {
      
      points.push_back(*begin);

      ++begin;
    }
  }

  bool compute(SvgContainer *) const;  
  
};

} // libg

#endif /* LIBG */
