#include <sstream>
#include "Style.hpp"
#include "SvgElement.hpp"

namespace libg {

Style &SvgElement::getStyle() {
  return style;
}

void SvgElement::setStyle (const Style &_style) {
  style = _style;
}

void SvgElement::setFilter(const std::string &filterName) {
  style.setFilter(filterName);
}


} // libg
