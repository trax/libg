#include <iterator>
#include "Line.hpp"


namespace libg {



Line::Line(X x0, Y y0, 
           X x1, Y y1) {
    
  points.push_back({x0, y0});
  points.push_back({x1, y1});
    
}

Line::Line(Point p0, Point p1) {
  points.push_back(p0);
  points.push_back(p1);
}

Line::Line(Points _points) {
  for (const auto point: _points){
    points.push_back(point);
  }
}


bool Line::compute(SvgContainer *container) const{
  if (points.size()  < 2){
    return false;
  } else if (points.size() == 2){
    (*container) << "<line " 
                 << "x1=\"" << points[0].x << "\" " 
                 << "y1=\"" << points[0].y << "\" "
                 << "x2=\"" << points[1].x << "\" "
                 << "y2=\"" << points[1].y << "\" ";
  } else {

    (*container) << "<polyline points=\"";

    for (const auto point : points){
      (*container) << point.x << "," 
                   << point.y << " ";
    }
    (*container) << "\" ";
  }

  (*container) << style << "/> ";
    

  return true;
}


} // libg
